#! /usr/bin/env node
const knex = require("knex")
const mysql = require("mysql2")
const worker = require("../index")
const fs = require("fs")
let args_ok = true

const optionDefinitions = {
  alias: {
    host: "h",
    username: "u",
    password: "p",
    database: "d",
    version: "v"
  },
  unknown: arg => {
    if (args_ok) {
      console.log("\nargument " + arg + " is not known")
    }
    args_ok = false
    return false
  }
}
const cmds = require("minimist")(process.argv.splice(2), optionDefinitions)
if (cmds._ && cmds._.length > 0) {
  usage()
} else {
  if (cmds.version) {
    const { version } = JSON.parse(fs.readFileSync("package.json"))
    console.log(`\nnormalize_mysqldb v. ${version}\n`)
  }
  let r = ""
  if (!cmds.username) {
    r += "\nusername (-u) not given"
  }
  if (!cmds.password) {
    r += "\npassword (-p) not given"
  }
  if (!cmds.database) {
    r += "\ndatabase name (-d) not given"
  }
  if (!cmds.host) {
    r += "\nhostname or address (-h) not given"
  }
  if (r || !args_ok) {
    usage(r)
  } else {
    const db = knex({
      client: "mysql2",
      connection: {
        user: cmds.username,
        password: cmds.password,
        host: cmds.host,
        database: cmds.database
      }
    })
    worker(db)
      .then(res => {
        console.log("finished")
        process.exit(0)
      })
      .catch(err => {
        console.log("Error: " + JSON.stringify(err))
        process.exit(-1)
      })
  }
}

function usage(errmsg) {
  if (errmsg) {
    console.log(errmsg + ".\n")
  }
  console.log(
    "usage: normalize_mysqldb -h <host> -u <user> -p <password> -d database\nor normalize_mysqldb -v\n"
  )
}
